# Compiling code_aster development version

One may want to use compile a more recent version of code_aster.
The container is ready for such usage as it comes along with all the prerequisites!

Summary

[[_TOC_]]

## Preparing the access to the container

To build older versions of code_aster, choose the container following the
[Changelog of Singularity Images and prerequisites](./devel/changelog.md).

It is important to work in the container since the environement is all prepared in
order to compile code_aster.

For sake of simpliciy, one might want to be able to use the container without actually
being in the container's directory. This can be done by adding the script to the
`$PATH` environment variable on the host machine. For instance, one can add
`$HOME/bin` to `$PATH` in one's `.bashrc` by simply adding this line :

```bash
export PATH=${HOME}/bin:${PATH}
```

Then, make a symbolic link to the container in `$HOME/bin` :

```bash
mkdir -p ${HOME}/bin
cd ${HOME}/bin
ln -s ${HOME}/containers/salome_meca-lgpl-2021.0.0-2-20211014-scibian-9 .
```

The last line implies that the container was installed in `${HOME}/containers/`.

To test it out, one can open a new terminal and determine whether the container
can be launched from any diretory:

```bash
salome_meca-lgpl-2021.0.0-2-20211014-scibian-9 --help
```

## Working *within* the container

It is important to be working in the container since the environment is all prepared
for a painless code_aster compilation *from scratch* . Thus, one must use a shell
session within the container :

```bash
salome_meca-lgpl-2021.0.0-2-20211014-scibian-9 --shell
```

## Creating the code_aster repositories

First, let's create a directory for the repositories:

```bash
mkdir -p $HOME/dev/codeaster
```

Then, let's clone the repositories from GitLab.com:

```bash
cd $HOME/dev/codeaster
git clone https://gitlab.com/codeaster/src.git
git clone https://gitlab.com/codeaster/devtools.git
```

> **NOTE:**
>
> In order to clone the repositories, one must be able to access https://gitlab.com.
> Thus, it may be necessary to set your proxy beforehand.
>
> This operation may take some time since code_aster has a long history!

## Building codeaster

In the container's shell, it is quite straightforward :

```bash
cd $HOME/dev/codeaster/src
./waf configure
./waf install -j 4
```

Parameter 4 implies that the code will be compiled in parallel, using 4 procs.
One may modify it to any other number, depending on the machine employed.

## What's different when using a container

A contain has its own environement and as a consequence, within the container,
one cannot access the system on the host machine. For instance, if you use a specific
text editor on your host machine, you cannot use it in the container!

Habits must this be changed : it it possible to use two terminal windows: one in the
container for compilation and another one, on the host machine in order to access de
source code with one's favorite editor. If needed, on can *bind* specific folders
in order to access them within the container.

For instance, if on wants to access a USB stick mounted on the host machine in
`/media/user/USB500`, one can use such command in order to enter the container with
access to the USB stick :

```bash
salome_meca-lgpl-2021.0.0-2-20211014-scibian-9 --bind /media/user/USB500 --shell
# the stick can now be accessed in /media/user/USB500...
```

and if one would rather mount it in `/data` instead :

```bash
salome_meca-lgpl-2021.0.0-2-20211014-scibian-9 --bind /media/user/USB500:/data --shell
# the stick can now be accessed in /data...
```

## Using the development version

Whether, you're using the *old school* astk or AsterStudy within salome_meca, the
development version is already made available in the container.
Simply choose `DEV` version.

However this automatic detection takes for granted that your installation is located
in `$HOME/dev/codeaster/install/std/share/aster` which is the usual directory if you
cloned the repositories in `$HOME/dev/codeaster`. If it's not the case, one only
has to modify the last line of the file `$HOME/.astkrc/prefs` (within the container
as it is located somewhere else on the host machine! ) accordingly.

In salome_meca you will not be prompted for a development version when starting
AsterStudy, but it will be available in the *History View* tab.

Enjoy!
