# salome_meca and code_aster open source documentation

This documentation aims at sharing between the different members of the
open source community of code_aster and salome_meca. It is open-source
and available on these repositories:

- [Installation and development][1]: Guides to install salome_meca, code_aster
and a *Getting started* for development (**this repository**).

- [Study Guides for salome_meca][2]: Collection of how to's, tutorials
and examples of salome_meca usages.

To read this pages on GitLab, one recommends to set in your GitLab account *Preferences*,
*Behavior* / *Project overview content* to *Readme*.

## Installation and Development

Documentation for the installation and development environment.

### Installation

- [Installation on Linux](./install/installation_linux.md)

  - [Installation of Singularity](./install/singularity_installation.md)

- [Installation on Windows](./install/installation_windows.md)

- [Using salome_meca](./usage/usage.md)

### Getting started for Development

- [Compiling code_aster development version](./devel/compile.md)

- [Changelog of Singularity Images and prerequisites](./devel/changelog.md)

- [Frequently Asked Questions](./usage/faq.md)

### Contributing

Anyone can contribute to this documentation as long as you follow the
rules. Since it is currently being put in place, the rules are not all
set yet. See [How to contribute](./contribute.md).

[1]: ../../../../opensource-installation-development
[2]: ../../../../opensource-documentation
